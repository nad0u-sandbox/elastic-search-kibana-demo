# Requirements

- .NET 5 (>= 5.0.200)
- docker (>= 19.03.13)
- docker-compose (>= 1.28.5)



# Before running docker-compose

In order for Elastic Search to run, you need to increase the `vm.max_map_count` of the JVM on the hosting machine:

```
sudo sysctl -w vm.max_map_count=262144
```

Otherwise, Elastic Search won't start (preventin Kibana to start as well) and will throw the following error:

```
es01     | {"type": "server", "timestamp": "2021-03-06T16:48:30,770Z", "level": "INFO", "component": "o.e.b.BootstrapChecks", "cluster.name": "es-docker-cluster", "node.name": "es01", "message": "bound or publishing to a non-loopback address, enforcing bootstrap checks" }
es01     | ERROR: [1] bootstrap checks failed
es01     | [1]: max virtual memory areas vm.max_map_count [65530] is too low, increase to at least [262144]
es01     | ERROR: Elasticsearch did not exit normally - check the logs at /usr/share/elasticsearch/logs/es-docker-cluster.log
es01     | {"type": "server", "timestamp": "2021-03-06T16:48:30,778Z", "level": "INFO", "component": "o.e.n.Node", "cluster.name": "es-docker-cluster", "node.name": "es01", "message": "stopping ..." }
es01     | {"type": "server", "timestamp": "2021-03-06T16:48:30,790Z", "level": "INFO", "component": "o.e.n.Node", "cluster.name": "es-docker-cluster", "node.name": "es01", "message": "stopped" }
es01     | {"type": "server", "timestamp": "2021-03-06T16:48:30,790Z", "level": "INFO", "component": "o.e.n.Node", "cluster.name": "es-docker-cluster", "node.name": "es01", "message": "closing ..." }
es01     | {"type": "server", "timestamp": "2021-03-06T16:48:30,801Z", "level": "INFO", "component": "o.e.n.Node", "cluster.name": "es-docker-cluster", "node.name": "es01", "message": "closed" }
es01     | {"type": "server", "timestamp": "2021-03-06T16:48:30,802Z", "level": "INFO", "component": "o.e.x.m.p.NativeController", "cluster.name": "es-docker-cluster", "node.name": "es01", "message": "Native controller process has stopped - no new native processes can be started" }
es01 exited with code 78
```

And Kibana complaining the following:

```
kib01    | {"type":"log","@timestamp":"2021-03-06T16:48:54Z","tags":["error","elasticsearch","monitoring"],"pid":6,"message":"Request error, retrying\nGET http://es01:9200/_xpack => getaddrinfo ENOTFOUND es01 es01:9200"}
kib01    | {"type":"log","@timestamp":"2021-03-06T16:48:54Z","tags":["warning","elasticsearch","monitoring"],"pid":6,"message":"Unable to revive connection: http://es01:9200/"}
kib01    | {"type":"log","@timestamp":"2021-03-06T16:48:54Z","tags":["warning","elasticsearch","monitoring"],"pid":6,"message":"No living connections"}
kib01    | {"type":"log","@timestamp":"2021-03-06T16:48:54Z","tags":["warning","plugins","licensing"],"pid":6,"message":"License information could not be obtained from Elasticsearch due to Error: No Living connections error"}
kib01    | {"type":"log","@timestamp":"2021-03-06T16:48:54Z","tags":["info","savedobjects-service"],"pid":6,"message":"Waiting until all Elasticsearch nodes are compatible with Kibana before starting saved objects migrations..."}
kib01    | {"type":"log","@timestamp":"2021-03-06T16:48:54Z","tags":["warning","elasticsearch","data"],"pid":6,"message":"Unable to revive connection: http://es01:9200/"}
kib01    | {"type":"log","@timestamp":"2021-03-06T16:48:54Z","tags":["warning","elasticsearch","data"],"pid":6,"message":"No living connections"}
kib01    | {"type":"log","@timestamp":"2021-03-06T16:48:54Z","tags":["error","savedobjects-service"],"pid":6,"message":"Unable to retrieve version information from Elasticsearch nodes."}
kib01    | {"type":"log","@timestamp":"2021-03-06T16:48:54Z","tags":["warning","elasticsearch","monitoring"],"pid":6,"message":"Unable to revive connection: http://es01:9200/"}
kib01    | {"type":"log","@timestamp":"2021-03-06T16:48:54Z","tags":["warning","elasticsearch","monitoring"],"pid":6,"message":"No living connections"}
kib01    | {"type":"log","@timestamp":"2021-03-06T16:48:54Z","tags":["warning","plugins","licensing"],"pid":6,"message":"License information could not be obtained from Elasticsearch due to Error: No Living connections error"}
kib01    | {"type":"log","@timestamp":"2021-03-06T16:48:54Z","tags":["warning","plugins","reporting","config"],"pid":6,"message":"Chromium sandbox provides an additional layer of protection, but is not supported for Linux Centos 7.8.2003 OS. Automatically setting 'xpack.reporting.capture.browser.chromium.disableSandbox: true'."}
kib01    | {"type":"log","@timestamp":"2021-03-06T16:48:57Z","tags":["warning","elasticsearch","data"],"pid":6,"message":"Unable to revive connection: http://es01:9200/"}
kib01    | {"type":"log","@timestamp":"2021-03-06T16:48:57Z","tags":["warning","elasticsearch","data"],"pid":6,"message":"No living connections"}
kib01    | {"type":"log","@timestamp":"2021-03-06T16:48:59Z","tags":["warning","elasticsearch","data"],"pid":6,"message":"Unable to revive connection: http://es01:9200/"}
kib01    | {"type":"log","@timestamp":"2021-03-06T16:48:59Z","tags":["warning","elasticsearch","data"],"pid":6,"message":"No living connections"}
kib01    | {"type":"log","@timestamp":"2021-03-06T16:49:02Z","tags":["warning","elasticsearch","data"],"pid":6,"message":"Unable to revive connection: http://es01:9200/"}
kib01    | {"type":"log","@timestamp":"2021-03-06T16:49:02Z","tags":["warning","elasticsearch","data"],"pid":6,"message":"No living connections"}
```

# Elastic Search and Kibana URLs

Elastic Search : [http://localhost:9200]()

Kibana : [http://localhost:5601]()

