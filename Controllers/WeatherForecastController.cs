﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Serilog;

namespace elastic_search_kibana_demo.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger _logger;

        public WeatherForecastController(ILogger logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                var rng = new Random();

                if (rng.Next(0, 5) < 2)
                {
                    throw new Exception("Oops!");
                }

                return Ok(Enumerable.Range(1, 5).Select(index => new WeatherForecast
                    {
                        Date = DateTime.Now.AddDays(index),
                        TemperatureC = rng.Next(-20, 55),
                        Summary = Summaries[rng.Next(Summaries.Length)]
                    })
                    .ToArray());
            }
            catch(Exception ex)
            {
                // _logger.LogError(ex, "Something unexpected happened!");
                _logger.Error(ex, "Something unexpected happened! {MyCustomProp}", 50); // This property can be used to search/filter in Kibana as fields.MyCustomProp
                return new StatusCodeResult(500);
            }
            
        }
    }
}
